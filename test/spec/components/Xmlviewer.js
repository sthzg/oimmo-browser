'use strict';

describe('Xmlviewer', function () {
  var React = require('react/addons');
  var Xmlviewer, component;

  beforeEach(function () {
    Xmlviewer = require('../../../src/scripts/components/Xmlviewer.js');
    component = React.createElement(Xmlviewer);
  });

  it('should create a new instance of Xmlviewer', function () {
    expect(component).toBeDefined();
  });
});
