'use strict';

describe('Viewpane', function () {
  var React = require('react/addons');
  var Viewpane, component;

  beforeEach(function () {
    Viewpane = require('../../../src/scripts/components/Viewpane.js');
    component = React.createElement(Viewpane);
  });

  it('should create a new instance of Viewpane', function () {
    expect(component).toBeDefined();
  });
});
