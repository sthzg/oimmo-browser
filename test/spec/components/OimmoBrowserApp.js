'use strict';

describe('Main', function () {
  var React = require('react/addons');
  var OimmoBrowserApp, component;

  beforeEach(function () {
    var container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    OimmoBrowserApp = require('../../../src/scripts/components/OimmoBrowserApp.js');
    component = React.createElement(OimmoBrowserApp);
  });

  it('should create a new instance of OimmoBrowserApp', function () {
    expect(component).toBeDefined();
  });
});
