'use strict';

describe('Brand', function () {
  var React = require('react/addons');
  var Brand, component;

  beforeEach(function () {
    Brand = require('../../../src/scripts/components/Brand.js');
    component = React.createElement(Brand);
  });

  it('should create a new instance of Brand', function () {
    expect(component).toBeDefined();
  });
});
