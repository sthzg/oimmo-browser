'use strict';

describe('Fileselect', function () {
  var React = require('react/addons');
  var Fileselect, component;

  beforeEach(function () {
    Fileselect = require('../../../src/scripts/components/Fileselect.js');
    component = React.createElement(Fileselect);
  });

  it('should create a new instance of Fileselect', function () {
    expect(component).toBeDefined();
  });
});
