'use strict';

describe('Dropzone', function () {
  var React = require('react/addons');
  var Dropzone, component;

  beforeEach(function () {
    Dropzone = require('../../../src/scripts/components/Dropzone.js');
    component = React.createElement(Dropzone);
  });

  it('should create a new instance of Dropzone', function () {
    expect(component).toBeDefined();
  });
});
