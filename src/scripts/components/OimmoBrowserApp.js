'use strict';

var React = require('react/addons');
var Fileselect = require('./Fileselect');
var Brand = require('./Brand');
var XMLViewer = require('./Xmlviewer');
var Viewpane = require('./Viewpane');

// Export React so the devtools can find it
(window !== window.top ? window.top : window).React = React;

// CSS
require('../../styles/normalize.css');
require('../../styles/main.less');

var OimmoBrowserApp = React.createClass({
    getInitialState: function() {
        return {
            files: [],
            viewpaneContent: null
        }
    },

    handleFileAdd: function(file) {
        var that = this;
        var reader = new FileReader();

        reader.onload = function() {
            var text = reader.result;
            var files = that.state.files;
            var fileIndex = files.length;
            var key = 'xmlviewer' + fileIndex;

            var xmlviewer = (
                <XMLViewer
                    key={key}
                    fileIndex={fileIndex}
                    file={file}
                    reader={reader}
                    xml={text} />
            );

            files.push(xmlviewer);
            that.setState({
                files: files,
                viewpaneContent: files
            });
        };

        reader.readAsText(file);
    },

    render: function () {

        // Show big dropzone if no file has been added so far.
        var viewpaneContent = this.state.viewpaneContent;
        if (viewpaneContent == null) {
            viewpaneContent = (
                <div className="ob-dropzone-big" style={{alignItems: 'center'}}>
                    <Fileselect onHandleFileAdd={this.handleFileAdd} dropzonetext="Drop open immo XML file here" />
                </div>
            );
        }

        return (
            <div className='main' style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', position: 'absolute', width: '100%', height: '100%', overflow: 'hidden'}}>
                <div className='head' style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Brand />
                    <Fileselect onHandleFileAdd={this.handleFileAdd} />
                </div>
                <div className='canvas' style={{flex:1, position: 'relative', height: 'inherit', overflow: 'hidden'}}>
                    <Viewpane panes={viewpaneContent} key="xmlviewers" style={{height: 'inherit'}} />
                </div>
                <div className='footer' style={{}}>
                    From the Exploratorium, 2015
                </div>
            </div>
        );
    }
});

module.exports = OimmoBrowserApp;
