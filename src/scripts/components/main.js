var OimmoBrowserApp = require('./OimmoBrowserApp');
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;

var content = document.getElementById('content');

var Routes = (
  <Route handler={OimmoBrowserApp}>
    <Route name="/" handler={OimmoBrowserApp}/>
  </Route>
);

Router.run(Routes, function (Handler) {
  React.render(<Handler/>, content);
});
