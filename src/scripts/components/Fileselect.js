'use strict';

var React = require('react/addons');
var Dropzone = require('./Dropzone');

require('../../styles/Fileselect.less');

var Fileselect = React.createClass({
    handleFileAdd: function(file) {
        this.props.onHandleFileAdd(file);
    },
    render: function () {
        return (
            <Dropzone
                onHandleFileAdd={this.handleFileAdd}
                dropzonetext={this.props.dropzonetext}
            />
        );
    }
});

module.exports = Fileselect;

