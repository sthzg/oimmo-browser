'use strict';

var React = require('react/addons');

require('../../styles/Viewpane.less');

var Viewpane = React.createClass({

    componentDidMount: function() {
        this.getDOMNode().style.height = 'inherit';
    },

    render: function () {
        return (
            <div style={{display: 'flex'}}>
                {this.props.panes}
            </div>
        );
    }

});

module.exports = Viewpane;

