'use strict';

var React = require('react/addons');

require('../../styles/Xmlviewer.less');


var Xmlviewer = React.createClass({

    getInitialState: function() {
        return {
            xpathQuery: '',
            numProperties: null,
            editor: null,
            obFilterValue: '',
            isObexFiltered: false
        }
    },

    componentDidMount: function() {
        var parser = new DOMParser();
        var xmlDoc = parser.parseFromString(this.props.xml, "text/xml");

        var oSerializer = new XMLSerializer();

        var sPrettyXML = vkbeautify.xml(oSerializer.serializeToString(xmlDoc));

        var editor = ace.edit('xmleditor-' + this.props.fileIndex);
        editor.getSession().setMode('ace/mode/xml');
        editor.getSession().setTabSize(4);
        editor.setTheme('ace/theme/monokai');
        editor.setValue(sPrettyXML);
        editor.setReadOnly(true);
        editor.setShowPrintMargin(false);
        editor.clearSelection();

        var numProperties = document.evaluate("count(//*[local-name()='immobilie'])", xmlDoc).numberValue;

        this.setState({
            numProperties: numProperties,
            editor: editor
        });
    },

    changeObexFilter: function(ev) {
        this.setState({obFilterValue: ev.target.value});
    },

    filterByObex: function(ev) {
        ev.preventDefault();
        ev.stopPropagation();

        var parser = new DOMParser();
        var oSerializer = new XMLSerializer();

        this.setState({isObexFiltered: true});

        var xmlDoc = parser.parseFromString(this.props.xml, "text/xml");
        var property = document.evaluate(
            "//*[local-name()='objektnr_extern' and text()[contains(.,'" + this.state.obFilterValue + "')]]/../../.",
            xmlDoc
        );

        var match = property.iterateNext();

        if (match == null) {
            this.state.editor.setValue('No match');
        } else {
            var sPrettyXML = vkbeautify.xml(oSerializer.serializeToString(match));
            this.state.editor.setValue(sPrettyXML);
        }

        this.state.editor.clearSelection();
        this.state.editor.scrollToLine(1, true, false);
        this.state.editor.gotoLine(1);
    },

    removeFilterByObex: function(ev) {
        var parser = new DOMParser();
        var oSerializer = new XMLSerializer();

        this.setState({
            obFilterValue: '',
            isObexFiltered: false
        });

        var xmlDoc = parser.parseFromString(this.props.xml, "text/xml");

        var sPrettyXML = vkbeautify.xml(oSerializer.serializeToString(xmlDoc));
        this.state.editor.setValue(sPrettyXML);
        this.state.editor.clearSelection();
        this.state.editor.scrollToLine(1, true, false);
    },

    render: function () {
        var divStyle = {
            width: '100%',
            height: '100%',
            flex: 1
        };
        var id = 'xmleditor-' + this.props.fileIndex;
        var obFilterValue = this.state.obFilterValue;
        return (
            <div style={divStyle}>
                <div className="ob-xmlviewer-pane">
                    <div className="ob-file-tab-bar">

                        <strong>{this.props.file.name}</strong>

                        <input
                            className="ob-input-text ob-xmlviewer-xpath-searchbox"
                            placeholder="objektnr_extern"
                            type="text"
                            value={obFilterValue}
                            onChange={this.changeObexFilter} />

                        <a href="#"
                            onClick={this.filterByObex}
                            className="ob-input-btn">
                            Ok
                        </a>

                        {this.state.isObexFiltered ? this.renderRemoveObexFilterBtn() : null}

                    </div>
                    <div className="ob-stats-tab">
                        {this.state.numProperties} properties
                    </div>
                    <div style={{height: '100%', flex: 1}} id={id} />
                </div>
            </div>
        );
    },

    renderRemoveObexFilterBtn: function () {
        return (
            <a href="#"
                onClick={this.removeFilterByObex}
                className="ob-input-btn remove">
                X
            </a>
        );
    }

});

module.exports = Xmlviewer;
