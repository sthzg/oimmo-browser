'use strict';

var React = require('react/addons');

require('../../styles/Dropzone.less');

var Dropzone = React.createClass({

    handleDragEnter: function(ev) {
        ev.stopPropagation();
        ev.preventDefault();

        console.log('drag enter');
    },

    handleDragOver: function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
    },

    handleDragLeave: function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
    },

    handleDrop: function(ev) {
        ev.stopPropagation();
        ev.preventDefault();

        var dt = ev.dataTransfer;
        var files = dt.files;

        this.handleFiles(files);
    },

    handleFiles: function(files) {
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var fileType = /xml.*/;

            if (!file.type.match(fileType)) {
                console.log('File type not supported. Please drop an XML file.');
                continue;
            }

            this.props.onHandleFileAdd(file);
        }
    },

    render: function () {
        return (
            <div
                className="ob-dropzone"
                onDragEnter={this.handleDragEnter}
                onDragOver={this.handleDragOver}
                onDragLeave={this.handleDragLeave}
                onDrop={this.handleDrop}
            >
                <p className="ob-dropzonetext">{this.props.dropzonetext}</p>
            </div>
        );
    }
});

module.exports = Dropzone;

